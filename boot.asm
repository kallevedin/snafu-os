;------------------------------------------------------------------------------
;                        SEKTOR 1
;
; MBR is loaded by bios to 07C0:0000
; sector 1 is the bootsector (or MBR - Master Boot Record) and is suposed to
; load the snafu-kernel (core.asm).
;
; this is what the mbr does right now:
;    * puts the stack to 9000:0000
;    * enables A20 
;    * loads the rest of snafu's 8 sectors to 0000:1000.
;    * jumps to snafu.
;
;------------------------------------------------------------------------------


[org 0]
[bits 16]

;-----------------------------------------------

start:
	mov ax, 0x07c0
	mov ds, ax		; MBR is loaded by bios to 07C0:0000

	cli			; no int's when there is no stack!
	mov ax, 0x9000
	mov ss, ax		; put the stack to 9000:ffff = 9ffff
	mov sp, 0xffff		; (remember: it grows downwards)
	sti

	mov si, alive
	call write		; show that it works

	call a20		; enables the a20
	call load_os		; load snafu

	mov si, jumping
	call write

	db 0xea			; opcode for "jmp far"
	db 0x00, 0x10		; offset
	db 0x00, 0x00		; segment (data is in litle endian!)

	; nu �r processorn i snafu's k�d

;-----------------------------------------------
; and here we got all the functions..
;-----------------------------------------------

write:
	lodsb			; al = ds:si
	or al,al
	jz done			; if end of string..
	mov ah, 0x0e		; 0x0e = put char
	mov bx, 0x000F		; white letters on black bkgrnd.
	int 0x10
	jmp write
done:	ret


load_os:
	xor ax, ax
	int 0x13		; reset floppy
	jc load_os		; if something goes wrong, try again :p

	xor ax, ax
        mov es,ax
	mov bx, 0x1000		; read them to es:bx (0000:1000)
        mov ax,0x0218		; ah= read, al= 12 kilobytes or 0x18 sectors
        mov cx,0x0002		; cylinder = 0, sector 2+
        mov dx,0x0000		; head = 0, drive=0 (A:)
        int 0x13
        jc load_os		; try until it works
	ret

;---------( a20 is hijacked from polyOS )---
a20:
        cli
        xor cx, cx
clear_buf:
        in al, 64h              ; get input from keyboard status port
        test al, 02h            ; test the buffer full flag
        loopnz clear_buf        ; loop until buffer is empty
        mov al, 0D1h            ; keyboard: write to output port
        out 64h, al             ; output command to keyboard
clear_buf2:
        in al, 64h              ; wait 'till buffer is empty again
        test al, 02h
        loopnz clear_buf2
        mov al, 0dfh            ; keyboard: set A20
        out 60h, al             ; send it to the keyboard controller
        mov cx, 14h
wait_kbc:                       ; this is approx. a 25uS delay to wait
        out 0edh, ax            ; for the kb controler to execute our
        loop wait_kbc           ; command.
	sti
	ret

;-----------------------------------------------

alive	db "alive", 10, 13, 0
jumping	db "jmp -> snafu code", 10, 13, 0

        times 510-($-$$) db 0
        dw 0xAA55