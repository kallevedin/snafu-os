

;----------------------------------------

map_all_memory:
	pusha

	mov eax, [ramsize]

	shr eax, 12		; eax = number of pages that exist
	cmp eax, 1024
	jae .ok

	mov esi, string_2	; printf("\nyou may need more ram.")
	call print_string
.ok
	shr eax, 10		; eax = the number of pt we need.
	mov ebx, page_tab + 3
	mov edx, page_dir
.fill_pd
	mov [edx], ebx		; PD[x] -> [PT_x]
	add edx, 4
	dec eax
	jz .ok2
	add ebx, 4096
	jmp .fill_pd	

.ok2
	mov edi, page_tab
	mov eax, 3		; present, rwx, supervisor.
	mov ecx, [ramsize]
	shr ecx, 12		; ecx = pages to write
.fill_pt
	stosd
	add eax, 0x1000		; next 4kb (1000000000000b)
	inc ebx
	cmp ebx, 1024
	je .need_new_pt
.z	dec ecx
	jnz .fill_pt

	jmp .down

.need_new_pt
	; creates a new pt and markes the memory it occupies as owned by snafu.

	push eax
	push ebx

	push ecx
	dec ecx			; do not waste presious memory!
	jz .argh

	xor eax, eax
	mov al, [variable]
	add edi, 4096
	mov ebx, [ram_picture + eax]
	mov [ebx], byte -1
	inc al
	mov [variable], al
	pop ecx
	pop ebx
	pop eax
	jmp .z

.argh
	pop ecx
	pop ebx
	pop eax

.down
	popa
	ret



;----------------------------------------



alloc_physical:
	; allocates ONE physical 4kb memory-block and sets the owner id = al.
	; eax = the physical address to the memory-block that was allocated.

	push edi

	mov edi, ram_picture
.loop	cmp [edi], byte 0
	je .found
	inc edi
	jmp .loop

.found	mov [edi], al
        mov eax, edi
        sub eax, ram_picture
	shl eax, 12

	pop edi
	ret



;----------------------------------------



dealloc_physical:
	; deallocates the (eax)'th 4kb-block

	push eax
	shr eax, 12
	mov [ram_picture + eax], byte 0
	pop eax
	ret



;----------------------------------------



direct_probing:
	; probes extended memory (1mb+).. tries to write and then read
	; to each new page in ram. returns ram-size (in bytes) in eax.
	; has to be executed with pages turned off.

	push ebx
	push ecx

	mov eax, 0x100000	; start at 1 megabyte
	mov ebx, 0x4D		; test value (or whatever..)
.loop	
	mov [eax], ebx
	mov ecx, [eax]
	cmp ebx, ecx
	jne .stop		
	add eax, 0x1000
	jmp .loop
.stop
	pop ecx
	pop ebx
	ret



