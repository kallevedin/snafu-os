
SNAFU OS
========

**Yeah!**

This is a super-simple operating system I wrote when I was first learning about how computers works. I wrote it in notepad and used the nasm assembler and a programmed called RAWrite.exe to load it into a floppy disk.

It still assembles. On a modern computer, one could use yasm and dd instead of nasm and RAWrite.

All it does is: loads the next 8 sectors after the MBR from the floppy disk into RAM, initalizes and enters protected mode (32-bit mode), sets up the virtual memory/paging and identity-maps the first megabyte of RAM. It also prints out a bunch of diagnostics on the screen. It assumes your screen is compatible with the original CRT controller, which it probably still actually is.

I remember that I wrote code for context-switching between two processes, both of which were running in kernel (ring 0) mode. I can not find that version though. It was perhaps lost.

It can fairly easily be made to boot off PXE, just by removing (commenting out) the floppy disk "driver" in the MBR. Its easier to boot off the network than to boot off a floppy disk.

It was really quite nice learning how the CPU works in detail :)

This code have always been in the **public domain**.
