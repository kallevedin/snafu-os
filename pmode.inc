
;---------------------------------------------
; segment-type

present		equ 10000000b
ring0		equ 00000000b
ring1		equ 00100000b
ring2		equ 01000000b
ring3		equ 01100000b
selector	equ 00010000b	; selector or system-thing?
executable	equ 00001000b
expansion	equ 00000100b	; why? put this to zero.
writeable	equ 00000010b
readable	equ 00000010b
accessed	equ 00000001b	; used in paging

code_r0         equ 10011010b
data_r0         equ 10010010b
code_r3         equ 11111010b
data_r3         equ 11110010b

sys_task	equ 10001001b
usr_task	equ 11101001b

;---------------------------------------------
; segment-flags

granularity	equ 10000000b
big_bit		equ 01000000b
always_zero	equ 00000000b

default_flag	equ 11000000b

maxsize		equ 00001111b

defaultflag	equ 11001111b

;---------------------------------------------
; interrupt-access-byte

taskgate	equ 00000101b
intgate		equ 00001110b
trapgate	equ 00001111b
; ring0		equ 00000000b	it's the same as for segment-types
; ring1		equ 00100000b
; ring2		equ 01000000b
; ring3		equ 01100000b
; present	equ 10000000b	also the same..

sys_interrupt	equ present + ring0 + intgate
sys_trap	equ present + ring0 + trapgate

;---------------------------------------------
;
; structure for a segment:
;
;	dw	0		limit 15:0
;	dw	0		base 15:0
;	db	0		base 23:16
;	db	0		type
;	db	0		flags, limit 19:16
;	db	0		base 31:24
;
;---------------------------------------------
;
; info about PAGING
;
;_bit___does_________________
; 31:12 physical address
; 5     accessed
; 2     user(1) / supervisor(0)
; 1     read(1) / write(0)
; 0     present(1) / not present(0)
;




