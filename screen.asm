;
; snafu core text-functions..
; included in core.asm
;
; placing all these functions here makes it
; easier to read and understand snafu..
;
;----------------------------------------




set_text_pos:
	; al = x, ah = y.
	mov [screen_pos.x], al
	mov [screen_pos.y], ah
	ret

;----------------------------------------

clear_text:
	; al = text attribute
	push eax
	push edi
	mov ah, al
	mov al, " "
	mov edi, 0xB8000
.loop	mov [edi], ax
	add edi, 2
	cmp edi, 0xB8000+(80*25*2)
	jne .loop
	mov [screen_pos.x], byte 0
	mov [screen_pos.y], byte 0
	pop edi
	pop eax
	ret

;----------------------------------------

print_al:
	; writes the ascii content of al onto the screen.

	push edi

	mov edi, 0xb8000
	call get_screen_pointer
	mov [edi], al

	call inc_screen_pointer

	pop edi
	ret

;----------------------------------------

print_string:
	; writes the asciiz esi points at.

	push eax
	push esi
.loop
	mov al, [esi]
	cmp al, 0				; 0x00 = end of string
	je .end
	cmp al, 0x0D				; 0x0d = CR ( = \n )
	jne .no_cr
	call carriage_return
	jmp .a
.no_cr

	call print_al
.a	inc esi
	jmp .loop
.end
	pop esi
	pop eax
	ret

;----------------------------------------

carriage_return:
	push eax

	mov [screen_pos.x], byte 0
	mov al, [screen_pos.y]
	cmp al, 24
	jb .no_scroll

	call scroll
	jmp .end

.no_scroll
	inc al
	mov [screen_pos.y], al

.end	pop eax
	ret

;----------------------------------------

scroll:
	push eax
	push ecx
	push esi
	push edi

	cli
	mov esi, 0xb8000+(80*2)*1	; source is the row under
	mov edi, 0xb8000+(80*2)*0	; the destination.
	mov ecx, 25*80*2/4		; 25 rows, 80 columns, 2 bytes / char, 4 bytes per turn
	rep stosd

.loop	mov eax, [esi]
	sub ecx, 4
	mov [edi], eax
	jnz .loop

	pop edi
	pop esi
	pop ecx
	pop eax
	ret

;----------------------------------------

inc_screen_pointer:
	push eax
	cld

	mov al, [screen_pos.x]
	cmp al, 80
	jb .no_incy

	call carriage_return
	jmp .end

.no_incy
	inc al
	mov [screen_pos.x], al

.end	pop eax
	ret

;----------------------------------------

get_screen_pointer:
	; edi += screen_pos.x * 2 + screen_pos.y * 160
	push eax
	push ebx
	xor ebx, ebx

	mov bl, [screen_pos.x]
	shl ebx, 1
	add edi, ebx
	mov bl, [screen_pos.y]
	mov eax, 160
	mul bx
	add edi, eax

	pop ebx
	pop eax
	ret

;----------------------------------------

write_eax:
	; writes the hex content of eax onto the screen.

	push ebx

	mov ebx, eax
.loop
	rol eax, 8		; get leftmost byte on right side
	call write_al
	cmp eax, ebx		; loop until 
	jne .loop

	pop ebx
	ret

;----------------------------------------

write_al:
	; writes the hex content of al onto the screen.

	push eax
	push eax

	shr al, 4		; get left nybble
	call hexget		; al = 1st ascii
	call print_al

	pop eax

	call hexget		; al = 2nd ascii
	call print_al

	pop eax
	ret

;----------------------------------------

hexget:
	; returns the low nybble of eax represented in ascii, hexadecimal.

	and eax, 0x0000000F
	or eax,  0x00000030
	cmp eax, 0x39
	ja .add7
	ret
.add7	add eax, 7		; 3A + 7 = 41
	ret


