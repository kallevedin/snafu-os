
;
;   snafu core.
;
;   the core handles all the system-specific stuff, like gdt, idt, the
;   paging system, the memory alloc system, some small but important
;   functions and a few other things.
;


%include "pmode.inc"
%include "colors.inc"

[org 0x1000]			; 0000:1000

	mov ax, cs
	mov ds, ax
	mov ax, 0xB800
	mov es, ax

	xor ax, ax
	int 0x16		; wait for key

	cli

	db 0x66			; 32 bit
	lea bx, [gdtr]
	lgdt [bx]		; load gdt

	db 0x66			; 32 bit
	lea bx, [idtr]
	lidt [bx]		; load idt

	mov eax, cr0
	or al, 1		; set PE-bit (protection enable)
	mov cr0, eax

	db 0x66			; 32 bit
	db 0xEA			; far jump
	dd enter_pm		; the physical adress
	dw syscode		; syscode is ring 0

;
;   32-bit protected mode. the core _should_ rely only on its own code
;   and not have to get back into realmode in order to do anything.
;
;   some info:
;   gdt = a structure that is used by the processor to calculate
;   physical locations.
;   idt = like the old ivt. its the place where all the interrupts are
;   described, who owns them and so on..
;
;   in order to make things run faster, snafu stores all its data in
;   pages separate to pages that contain code.
;

[bits 32]
enter_pm:
	mov ax, sysdata
	mov ds, ax
	mov es, ax
	mov fs, ax
	mov gs, ax
	mov ss, ax
	mov esp, 0x9FFFF	; just below video-memory


	mov al, gray
	call clear_text

	mov esi, string_1
	call print_string
	call direct_probing
	call write_eax
	mov [ramsize], eax


	call map_all_memory	; maps ALL physical memory with paging..


	mov eax, page_dir
	mov cr3, eax		; install pages

	mov eax, cr0
	or eax, 0x80000000	; set PG-bit (enables paging)
	mov cr0, eax

	db 0xEA			; far jump
	dd enter_pageworld	; the physical adress
	dw syscode		; syscode is ring 0

enter_pageworld:

	sti

	mov al, 0x11		; put both 8259s in init mode
	out 0x20, al
	out 0xA0, al
	mov al, 0x20		; IRQ0-IRQ7 -> int 0x20-27
	out 0x21, al
	mov al, 0x28
	out 0xA1, al		; IRQ8-IRQ15 -> int 0x28-30
	mov al, 4
	out 0x21, al
	mov al, 2
	out 0xA1, al
	mov al, 1
	out 0x21, al
	out 0xA1, al

	mov eax, 11931		; 100,007 Hz
	call irq0_speed

	mov esi, welcome_1
	call print_string

circle:	mov bl, [key]
	cmp bl, 0x1B		; 1B = escape
	je .killme
	mov [es:0xB809C], bl

	jmp circle

.killme jmp reboot

;------ [ some functions ]----------------------------

	%include "screen.asm"		; a lot of text-mode functions.
	%include "memory.asm"		; memory & paging functions

reboot:
	in al, 0x64
	test al, 0x02
	jnz reboot
	mov al, 0xFC
	out 0x64, al
	jmp $


put_eip_on_stack:
	; puts the callers current eip on the stack.

	mov [variable], eax
	mov [esp+4], eax
	push eax			; there are two return addr. on the stack now.
	mov eax, [variable]
	ret				; one ret-addr left for the caller.


irq_clear:
	push ax
	mov al, 0x20
	out 0x20, al		; quiet screaming irq chip.
	pop ax
	ret


irq0_speed:			; 1193180 / ax = speed
	push eax
	mov al, 0x3C		; 0x3C = bin-wave; low byte first; counter 0 select
	out 0x43, al		; 0x43 = 8253 timer chip
	pop eax
	out 0x40, al		; the low byte of ax
	shr ax, 8
	out 0x40, al		; then the high byte of ax
	ret


;------ [ interrupt service routines ]----------------

unhandled_int:	
	mov byte [es:0xB809E], "U"
	iret

nmi_interrupt:
	mov byte [es:0xB809E], "N"
	iret			; this is NOT a nice solution

clock_tick:
	mov byte [es:0xB809E], "C"
	call irq_clear
	iret

keyboard:
	cli
	cld
	push eax
	push edi

	in al, 0x60
	cmp al, 29		; ctrl down
	jne .noctrldwn
	mov byte [ctrl_key], 0xff
.noctrldwn:
	cmp al, 29+128		; ctrl up
	jne .noctrlup
	mov byte [ctrl_key], 0x00
.noctrlup:
	cmp al, 56		; alt down
	jne .noaltdwn
	mov byte [alt_key], 0xff
.noaltdwn:
	cmp al, 56+128		; alt up
	jne .noaltup
	mov byte [alt_key], 0x00
.noaltup:
	cmp al, 42		; left shift down
	jne .nolshftdwn
	mov byte [lshift_key], 0xff
.nolshftdwn:
	cmp al, 42+128		; left shift up
	jne .nolshftup
	mov byte [lshift_key], 0x00
.nolshftup:
	cmp al, 54		; right shift down
	jne .norshftdwn
	mov byte [rshift_key], 0xff
.norshftdwn:
	cmp al, 54+128		; right shift up
	jne .norshftup
	mov byte [rshift_key], 0x00
.norshftup:
	cmp al, 128		; if below 128, then there is a key
	jae nokey

	mov edi, eax

	cmp byte [rshift_key], 0xff
	je kshift
	cmp byte [lshift_key], 0xff
	je kshift
	cmp byte [ctrl_key], 0xff
	je kctrl
	cmp byte [alt_key], 0xff
	je kalt

; normal tangent:

	mov al, [edi+normal_keymap]
	jmp keydone
kshift:
	mov al, [edi+shift_keymap]
	jmp keydone
kctrl:
	mov al, [edi+normal_keymap]
	sub al, 0x60
	jmp keydone
kalt:
	mov al, [edi+alt_keymap]
keydone:
	mov [key], al

nokey:
	mov al,0x20		; clear the irq-buffer
	out 0x20, al

	pop edi
	pop eax
	sti
	iret


page_fault:
	mov byte [es:0xB809C], "P"
	iret

div_error:
	mov byte [es:0xB809C], "D"
	iret

debug_exception:
	mov byte [es:0xB809C], "d"
	iret

int3_trap:
	mov byte [es:0xB809C], "3"
	iret

into_trap:
	mov byte [es:0xB809C], "O"
	iret

bound_trap:
	mov byte [es:0xB809C], "B"
	iret

invalid_instruction:
	mov byte [es:0xB809C], "I"
	iret

no_coprocessor:
	mov byte [es:0xB809C], "n"
	iret

double_fault:
	mov byte [es:0xB809C], "8"
	iret

coprocessor_segment_overrun:
	mov byte [es:0xB809C], "9"
	iret

invalid_tss:
	mov byte [es:0xB809C], "A"
	iret

segment_not_present:
	mov byte [es:0xB809C], "B"
	iret

stack_fault:
	mov byte [es:0xB809C], "C"
	iret

gpf:
	mov byte [es:0xB809C], "D"
	iret

coprocessor_error:
	mov byte [es:0xB809C], "e"
	iret

alignment_check:
	mov byte [es:0xB809C], "F"
	iret

machine_check:
	mov byte [es:0xB809C], "G"
	iret

unhandled_irq:
	mov byte [es:0xB809C], "Q"
	call irq_clear
	iret

;------------------------------------------------------------------------------
; data area. this page is used to store misc data.
; 

align 0x1000, db 0



idtr:	dw idt_end - idt -1
	dd idt
	dw 0

ramsize		dd 0

rshift_key	db 0
lshift_key	db 0
ctrl_key	db 0
alt_key		db 0
key		db 0		; <-- last key pressed

screen_pos:
	.x	db 0
	.y	db 0

variable:	dd 7		; do not trust this value!!!
string_1	db "ram size is: ",0
string_2	db 0x0d, "warning: you may need more ram.",0
welcome_1	db 0x0d, "paging is on.",0

%include "keymaps.inc"

; the ram picture contains a string of bytes wich mark each physical 4kb entry
; as either used or not used. 0 = unused, -1 = system, 1 to 0xFE = process ID
; that owns the page. (yes, that means that snafu only suports 253 simultaneus
; processes..)

align 16, db 0
ram_picture:
		db -1		; IVT & BIOS DATA AREA
		db -1		; core code
		db -1		; core data (this part)
		db -1		; core idt & gdt
		db -1		; the core's page directory
		db -1		; the core's page table (for 0-4mb)
   %rep 153
                db 0            ; <-- FREE RAM!
   %endrep
		db -1		; snafu stack
   %rep 96
                db -1           ; VIDEO, BIOS ROM, ROM
   %endrep



;------------------------------------------------------------------------------
align 0x1000, db 0

idt:

; int 0
	dw div_error		; div error
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 1
	dw debug_exception	; debug exception
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2
	dw nmi_interrupt	; non maskable interrupt
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 3
	dw int3_trap		; int3 trap
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 4
	dw into_trap		; into trap
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 5
	dw bound_trap		; bound trap
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 6
	dw invalid_instruction	; invalid instruction
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 7
	dw no_coprocessor	; no coprocessor
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 8
	dw double_fault		; double fault
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 9
	dw coprocessor_segment_overrun	; coprocessor segment overrun 
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int A
	dw invalid_tss		; invalid tss
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int B
	dw segment_not_present	; segment not present
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int C
	dw stack_fault		; stack fault
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int D
	dw gpf			; general protection fault
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int E
	dw page_fault		; page fault
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int F
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 10
	dw coprocessor_error	; coprocessor error
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 11
	dw alignment_check	; alignment check
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 12
	dw machine_check	; machine check 
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 13
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 14
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 15
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 16
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 17
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 18
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 19
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 1A
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 1B
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 1C
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 1D
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 1E
	dw unhandled_int	; reserved
	dw syscode		
	db 0
	db sys_interrupt
	dw 0

; int 1F
	dw unhandled_int	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 20 <- IRQ 0
	dw clock_tick		; timer
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 21 <- IRQ 1
	dw keyboard		; keyboard
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 22 <- IRQ 2
	dw unhandled_irq	; reserved (8259B)
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 23 <- IRQ 3
	dw unhandled_irq	; COM 1,3
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 24 <- IRQ 4
	dw unhandled_irq	; COM 2,4
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 25 <- IRQ 5
	dw unhandled_irq	; LTP
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 26 <- IRQ 6
	dw unhandled_irq	; primary IDE drives (floppy)
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 27 <- IRQ 7
	dw unhandled_irq	; secondary IDE drives (hdd?)
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 28 <- IRQ 8
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 29 <- IRQ 9
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2A <- IRQ A
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2B <- IRQ B
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2C <- IRQ C
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2C <- IRQ C
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2D <- IRQ D
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2E <- IRQ E
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

; int 2F <- IRQ F
	dw unhandled_irq	; reserved
	dw syscode
	db 0
	db sys_interrupt
	dw 0

%rep 0xFF - 0x2F
	dw unhandled_int	; software interrupts
	dw syscode
	db 0
	db sys_interrupt
	dw 0
%endrep

idt_end:

;------------------------------------------------------------------------------

align 16, db 0

gdt:
gdtr:	dw gdt_end - gdt - 1	; size of gdt
	dd gdt			; gdt base (physical adress)
	dw 0			; align to 8 bytes (dw + dd + dw = 8)

  syscode equ $-gdt		; 0x0008
gdt2:
        dw     0xffff
        dw     0x0000
        db     0x00
	db     code_r0
	db     defaultflag
        db     0x00

  sysdata equ $-gdt		; 0x0010
gdt3:
        dw     0xffff
        dw     0x0000
        db     0x00
	db     data_r0
	db     defaultflag
        db     0x00

  taskcode equ $-gdt		; 0x0018
gdt4:
        dw     0xffff
        dw     0x0000
        db     0x00
	db     data_r3
	db     defaultflag
        db     0x00

  taskdata equ $-gdt		; 0x0020
gdt5:
        dw     0xffff
        dw     0x0000
        db     0x00
	db     data_r3
	db     defaultflag
        db     0x00

gdt_end:
;------------------------------------------------------------------------------

	; smooth it out..
	align 0x1000, db 0

end_of_core:


page_dir	equ	end_of_core
page_tab	equ	end_of_core + 0x1000
